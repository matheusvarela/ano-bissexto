<?php
// Agradeço a DEUS pelo dom do conhecimento

class AnoBissexto
{
	public function calcularAnoBissexto(int $ano)
	{
		if (is_integer($ano) && $ano > 0)
		{
			if (($ano % 4 == 0) && ($ano % 100 != 0) || ($ano % 400 == 0))
			{
				return 'é bissexto';
			}
			else
			{
				return 'não é bissexto';
			}
		}
		else
		{
			throw new Exception('Ano inválido');
		}
	}
}
