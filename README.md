Classes de equivalencia

| Variáveis de Entrada |        Condições	  |                        Classes Válidas                          |           Classes Inválidas             | Saída Esperada  |
|----------------------|--------------------------|-----------------------------------------------------------------|--------------------------------------------------------------|-----------------|
|         Ano          | Número Inteiro Positivo  |	                        Ano > 0                             |                            Ano <= 0                 |      Ano válido |
|                      |                          | Ano divisivel por 4 e não divisivel de 100 ou divisivel por 400 | não divisivel por 4 e divisivel por 100 e não divisivel por 400  | Ano bissexto   |

Tabela para condições válidas

| Variável de entrada  |
|----------------------|
|       Ano = 1600     |
|       Ano = 2008     |


Tabela para condições inválidas

|  Variável de entrada |
|----------------------|
|       Ano = 1951     |
|       Ano = 2011     |
