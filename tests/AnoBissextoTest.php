<?php
// Agradeço a DEUS pelo dom do conhecimento

use PHPUnit\Framework\TestCase;

/**
 * @covers AnoBissexto
 */
final class AnoBissextoTest extends TestCase
{

	private $ano;

	public function testCalcularAnoBissextoValido1()
	{
		$this->ano = 1600;
		$this->assertEquals(
			'é bissexto',
			AnoBissexto::calcularAnoBissexto($this->ano)
	        );
	}

       public function testCalcularAnoBissextoValido2()
       {
                $this->ano = 2008;
                $this->assertEquals(
                        'é bissexto',
                        AnoBissexto::calcularAnoBissexto($this->ano)
                );

        }

	public function testCalcularAnoBissextoInvalido1()
        {
		$this->ano = 0;
		$this->expectException(Exception::class);
		AnoBissexto::calcularAnoBissexto($this->ano);
        }

	public function testCalcularAnoBissextoInvalido2()
        {
		$this->ano = 1951;
                $this->assertEquals(
                        'não é bissexto',
                        AnoBissexto::calcularAnoBissexto($this->ano)
                );
	}

	public function testCalcularAnoBissextoInvalido3()
        {
		$this->ano = 2011;
                $this->assertEquals(
                        'não é bissexto',
                        AnoBissexto::calcularAnoBissexto($this->ano)
                );
        }
}
